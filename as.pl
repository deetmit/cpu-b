#!/usr/bin/perl
use Data::Dumper;

sub trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };
sub n
{
    my ($n) = @_;
    local $_ = trim($n);
    
    if(/^([0-9a-fA-F]+)[Hh]$/){ return hex($1); }
    if(/^0x([0-9a-fA-F]+)$/){ return hex($1); }
    if(/^([01]+)b$/){ return oct("0b".$1);}
    if(/^[0-9]+$/){ return int($n); }
    
    
    print "error: wrong number format ($n)\n";
    exit(1);
}

sub isnumber
{
    my ($n) = @_;
    local $_ = trim($n);
    
    if(/^([0-9a-fA-F]+)[Hh]$/){ return 1; }
    if(/^0x([0-9a-fA-F]+)$/){ return 1; }
    if(/^([01]+)b$/){ return 1;}
    if(/^[0-9]+$/){ return 1; }
    
    return 0;

}

sub isregister
{
    my ($r) = @_;
    local $_ = $r;
    if(/[a-hA-H]/){return 1;}else{return 0;}
}

sub r
{
    my ($r) = @_;
    local $_ = $r;
    if(/[a-h]/){
	return ord($r)-ord('a');
    }elsif(/[A-H]/){
	return ord($r)-ord('A');
    }else{
	print "error: wrong register '$r'\n";
	exit(1);
    }
}



=pod
preprocessor

\s*%include\s+(.*)$
$1 == filename


=cut

@filecontent = ();

sub preprocessor
{
    my ($file,$fromfile,$i) = @_;
    
    open(f,"<",$file) or die "Cannot open file '$file'\n";
    my @ls = <f>;
    close(f);
    my $cont = join('', @ls);
    foreach my $f (@filecontent) {
	if($f eq $cont) {
	    print "error: recursive %include '$file' in file '$fromfile' in line $i\n";
	    exit(1);
	}
    }
    push(@filecontent,$cont);    


    my @out = ();
    my $i=1;
    foreach my $l (@ls){
	if($l=~/^\s*%include\s+(.+)$/){
	    my $fincude = trim($1);
	    push(@out, preprocessor($fincude,$file,$i));
	}else{
	    push(@out,$l);
	}
	++$i;
    }
    return @out;
}


my @lines =();

my ($filein,$fileout) = @ARGV;

if( (defined $filein) and (defined $fileout) ){
#    #reading from file
#    open(f,"<", $filein) or die "Cannot open file '$file'\n";
#    @lines = <f>;
#    close f;
    @lines = preprocessor($filein,"<bash>");
}else{
    print "error: wrong arguments\n";
    print "Usage: ./as.pl <file.asm> <file.bin>\n";
    exit(1);
}



my %db = ();    
my %label = ();
my %err = ();    
    
sub islabel
{
    my ($L) = @_;
    foreach my $l (keys %label){
	if( $l eq $L ) { return 1; }
    }
    return 0;
}    

sub addlabel
{
    my ($l,$nr,$addr) = @_;
    if( islabel($l) ) {
	print "error: duplicate label '$l' in line $nr\n";
	exit(1);
    }else{
	$label{$l} = $addr;
	$label{"$l#l"} = $addr&0xff;
	$label{"$l#h"} = ($addr>>8)& 0xff;
    }
}
my $REG = "[a-hA-H]";
my $HEX = '0x[0-9a-fA-F]+';
my $BIN = '[01]+b';
my $DEC = '[0-9]+';
my $NUMBER = "(?:$HEX|$BIN|$DEC)";
my $LABELDEF = '[a-zA-Z_][A-Za-z_0-9]*';
my $LABELREAD = "$LABELDEF(?:#[hl])?";
my $VALUE = "(?:$LABELREAD|$NUMBER)";

my $COMMENT = '(?:;.*)';
    
    
    
sub parse_db
{
    my ($wholeline,$l,$nr,$addr) = @_;
    my @bins = ();
    my @elist = split(/,/,$l);
    foreach my $e (@elist){
	$te=trim($e);
	if($te=~/^($NUMBER)(?:\s+times\s+($NUMBER))?$/){
	    if(defined $2) {
		foreach ( 1 .. n($2) ){
		    push(@bins,n($1));
		}
	    }else{
		push(@bins,n($1));
		
	    }
	}elsif($te=~/^'(.)'(?:\s+times\s+($NUMBER))?$/){
	    if(defined $2){
		foreach ( 1 .. n($2) ){
		    push(@bins,ord($1) );
		}
	    }else{
		push(@bins, ord($1));
	    }	    
	}elsif($te=~/^"([^"]*)"(?:\s+times\s+($NUMBER))?$/){ #"
	    my @lbins = ();
	    foreach $c ( split(//,$1) ){
		push (@lbins, ord($c));
	    }
	    if(defined $2){
		foreach ( 1 .. n($2) ){
		    push(@bins, @lbins) ;
		}
	    }else{
		push(@bins, @lbins);
	    }
	}
    }
    return {nr=>$nr,line=>$wholeline,size => scalar( @bins), bin => [@bins], complete=>1};
}    
    
sub parse_line
{
    my ($line,$nr,$addr) = @_;
    
#print "parse_line($line,$nr,$addr)\n";
    $line = trim($line);
    local $_ = trim($line);
    
    
    
    if(/^;.*$/){ 
	#comment
	return { nr=>$nr, size => 0, complete=>1, line => $line };
    }elsif(/^\s*$/){
	return {nr=>$nr,line=>$line, size=>0, complete=>1};
    }elsif(/^($LABELDEF):\s*$COMMENT?$/){
	# label:
	addlabel($1,$nr,$addr);
	return {line=>$line, nr=>$nr, size=>0, complete=>1};
    }elsif(/^db\s+(.*)$/){ 
	# db 
	return parse_db($line,$1,$nr,$addr);
    }elsif(/^nop\s*(:?$COMMENT)?$/){
	#nop
	return { nr=>$nr, bin => [0x0], size=>1, complete=>1, line => $line};
    }elsif(/^mov\s+($REG)\s+($NUMBER)\s*$COMMENT?$/){
	#mov r n
	my $v = n($2);
	my $r = r($1);
	return { nr=>$nr, size=>2, complete=>1, line => $line, bin => [0x40+$r,$v]};


    }elsif(/^mov\s+($REG)\s+($REG)\s*$COMMENT?$/){
	# mov r r
	return {line=>$line, nr=>$nr, complete=>1, size => 1, bin => [0x80+8*r($1)+r($2)]};


    }elsif(/^mov\s+($REG)\s+($LABELREAD)\s*$COMMENT?$/){
	#mov r label
	my $l = $2;
	my $r = r($1);
	
	if( islabel($2) ){
	    return { nr=>$nr, size=>2, complete=>1, line => $line, bin => [0x40+$r,$label{$l}]};
	}else{
	    return { nr=>$nr, size=>2, complete=>0, line => $line};
	}


    }elsif(/^mov\s+($REG)\s+\[\s*($VALUE)\s*]\s*$COMMENT?$/){
	#mov r [M]
	my $r = $1;
	my $v = $2;
	if(isnumber($v)){
	    return {nr=>$nr,size=>3, line=>$line, bin => [ 0x48+r($r), 0xff&n($v),  (n($v)>>8)&0xff ], complete=>1 };
	}elsif(islabel($v)){	    
	    my $Ml = 0xff & $label{$v};
	    my $Mh = ($label{$v}>>8)& 0xff;
	    return {nr=>$nr,size=>3, line=>$line, bin => [ 0x48+r($r), $Ml, $Mh ] , complete=>1};
	}else{
	    return {nr=>$nr,size=>3, line=>$line, complete=>0 };	
	}
    }elsif(/^mov\s+\[\s*($VALUE)\s*\]\s+($REG)\s*$COMMENT?$/){
	# mov [M] r
	my $M = $1;
	my $r = r($2);
	if(isnumber($M)){
	    my $Ml = 0xff & n($M);
	    my $Mh = (n($M)>>8)&0xff;
	    return {nr=>$nr,line=>$line,complete=>1,size=>3, bin => [ 0x50+$r, $Ml, $Mh ] };
	}elsif(islabel($M)){
	    my $Ml = 0xff & ($label{$M});
	    my $Mh = ($label{$M}>>8)&0xff;
	    return {nr=>$nr,line=>$line,complete=>1,size=>3, bin => [ 0x50+$r, $Ml, $Mh ] };
	}else{
	    return {nr=>$nr,line=>$line,complete=>0,size=>3 };
	}
	
    }elsif(/^mov\s+\[\s*($VALUE)\s*\]\s+($VALUE)\s*$COMMENT?$/){
	# mov [M] v
	my $M = $1;
	my $v = $2;
	if(isnumber($v)){
	    $v = n($v);
	}elsif(islabel($v)){
	    $v = $label{$v};
	}else{
	    return {nr=>$nr,line=>$line,complete=>0,size=>4 };	    
	}
	if(isnumber($M)){
	    my $Ml = 0xff & n($M);
	    my $Mh = (n($M)>>8)&0xff;
	    return {nr=>$nr,line=>$line,complete=>1,size=>4, bin => [ 0x1, $Ml, $Mh, $v ] };
	}elsif(islabel($M)){
	    my $Ml = 0xff & ($label{$M});
	    my $Mh = ($label{$M}>>8)&0xff;
	    return {nr=>$nr,line=>$line,complete=>1,size=>4, bin => [ 0x1, $Ml, $Mh, $v ] };
	}else{
	    return {nr=>$nr,line=>$line,complete=>0,size=>4 };
	}
    }elsif(/^mov\s+($REG)\s+\[[hH]:[gG]\]\s*$COMMENT?$/){
	my $r = r($1);
	return {nr=>$nr,line=>$line,complete=>1,size=>1,bin=>[0x58+$r]};
    }elsif(/^mov\s+\[[hH]:[gG]\]\s+($REG)\s*$COMMENT?$/){
	my $r = r($1);
	return {nr=>$nr,line=>$line,complete=>1,size=>1,bin=>[0x60+$r]};
    }elsif(/^setstack\s*$COMMENT?$/){
	return {nr=>$nr,line=>$line,complete=>1,size=>1,bin=>[0x2]};
    }elsif(/^push\s+($NUMBER)\s*$COMMENT?$/){
	return {nr=>$nr,line=>$line, complete=>1, size=>2, bin=>[0x3, n($1)]};    	
    }elsif(/^push\s+($REG)\s*$COMMENT?$/){
	return {nr=>$nr,line=>$line, complete=>1, size=>1, bin=>[0x68 + r($1)]};
    }elsif(/^pop\s+($REG)\s*$COMMENT?$/){
	return {nr=>$nr,line=>$line, complete=>1, size=>1, bin=>[0x70+r($1)]};
    }elsif(/^call\s+($VALUE)\s*$COMMENT?$/){
	#call A
	my $A = $1;
	if( isnumber($A) ){
	    my $Al = $A & 0xff;
	    my $Ah = ($A >> 8 ) & 0xff;
	    return {nr=>$nr,line=>$line, complete=>1, size=>3, bin=>[0x4, $Al, $Ah]};
	}elsif(islabel($A)){
	    my $Al = $label{$A} & 0xff;
	    my $Ah = ($label{$A} >>8) & 0xff;
	    return {nr=>$nr,line=>$line, complete=>1, size=>3, bin=>[0x4, $Al, $Ah]};	    
	}else{
	    return {nr=>$nr,line=>$line, complete=>0, size=>3};
	}
    }elsif(/^call\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x5] };	
    }elsif(/^ret\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x6] };	
    }elsif(/^irqset\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x7] };	
    }elsif(/^irqen\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x8] };	
    }elsif(/^irqdis\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x9] };	
    }elsif(/^irqret\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0xa] };	
    }elsif(/^add\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0xb] };	
    }elsif(/^sub\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0xc] };	
    }elsif(/^and\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0xd] };	
    }elsif(/^or\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0xe] };	
    }elsif(/^xor\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0xf] };	
    }elsif(/^not\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x10] };	
    }elsif(/^cmpu>\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x11] };	
    }elsif(/^cmpu=\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x12] };	
    }elsif(/^cmpu<\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x13] };	

    }elsif(/^cmps>\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x14] };	
    }elsif(/^cmps=\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x15] };	
    }elsif(/^cmps<\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x16] };	

    }elsif(/^pushf\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x17] };	
    }elsif(/^popf\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x18] };	

    }elsif(/^jmpabs\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x19] };	
	    
    }elsif(/^jmprel\s*$COMMENT?$/){
	    return {nr=>$nr,line=>$line, complete=>1, size=>1, bin => [0x1a] };	

    }elsif(/^jmpif\s*($VALUE)\s*$COMMENT?$/){
	#jmpif A
	my $A = $1;
	if( isnumber($A) ){
	    my $Al = $A & 0xff;
	    my $Ah = ($A >> 8 ) & 0xff;
	    return {nr=>$nr,line=>$line, complete=>1, size=>3, bin=>[0x1b, $Al, $Ah]};
	}elsif(islabel($A)){
	    my $Al = $label{$A} & 0xff;
	    my $Ah = ($label{$A} >>8) & 0xff;
	    return {nr=>$nr,line=>$line, complete=>1, size=>3, bin=>[0x1b, $Al, $Ah]};	    
	}else{
	    return {nr=>$nr,line=>$line, complete=>0, size=>3};
	}

    }elsif(/^jmp\s*($VALUE)\s*$COMMENT?$/){
	#jmpif A
	my $A = $1;
	if( isnumber($A) ){
	    my $Al = $A & 0xff;
	    my $Ah = ($A >> 8 ) & 0xff;
	    return {nr=>$nr,line=>$line, complete=>1, size=>3, bin=>[0x1c, $Al, $Ah]};
	}elsif(islabel($A)){
	    my $Al = $label{$A} & 0xff;
	    my $Ah = ($label{$A} >>8) & 0xff;
	    return {nr=>$nr,line=>$line, complete=>1, size=>3, bin=>[0x1c, $Al, $Ah]};	    
	}else{
	    return {nr=>$nr,line=>$line, complete=>0, size=>3};
	}


    }else{
     print "error: $line\n";
	return { nr=>$nr, size => 0, error=>"unrecognised" , complete=>0 , line => $line};
    }
}


my $nr = 1;    
my $addr = 0;
foreach my $line (@lines){
    my $tline = trim($line);
    if($tline=~/^offset\s+([+-])?($NUMBER)\s*$COMMENT?$/){
	my $sign = $1;
	my $number = $2;
	if( $sign =~ /\+/ ){
	    $addr+=n($number);
	}elsif( $sign =~ /-/ ){
	    $addr-=n($number);
	}else{
	    $addr=n($number);
	}
	next;
    }

    my $ldb_p = parse_line($tline,$nr,$addr);
    my %ldb = %$ldb_p;
    if( $ldb{size} > 0 ){
        $db{$addr} = $ldb_p;
	$addr+= $ldb{size};
    }
    if(exists $ldb{error}) {
	$err{$nr} = $ldb_p;
    }

    $nr++;
}	

#print Dumper (\%db);
#print Dumper (\%label);
foreach my $e (keys %db){
    if( $db{$e}{complete} == 0 ) {
	my $ldb_p = parse_line($db{$e}{line},$db{$e}{nr},$e);
	my %ldb = %$ldb_p;
	if( $ldb{complete} == 0 ) {
	    print "error: compilation error, cannot resolve line '$ldb{line}' in line $ldb{nr}\n";
	    exit(1);
	}else{
	    $db{$e} = $ldb_p;
	}
	
    }
}




#print Dumper(\%db);
#print Dumper(\%label);
#print Dumper(\%err);

my @l = sort {$a <=> $b} keys %db;
my $last = $l[$#l];
#print "last=$last\n";
open(f,">",$fileout);
printf f "v2.0 raw\n";
my @bin = ();
for(my $i=0;$i<=$last;$i++){
    if(exists $db{$i}) { 
	my $bin_p = $db{$i}{bin};
	my @bin_l = @$bin_p;
	push(@bin,@bin_l);
	$i+=$db{$i}{size};
	$i--;
    }else{
	push(@bin,0);
    }
}
foreach my $b (@bin){
    printf f "%x ", $b;
}
#print "bin=@bin\n";
close f;