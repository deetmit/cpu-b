;
;init
;

;set stack
    mov h stack#h
    mov g stack#l
    setstack
    
;set irq address
    mov h irq0#h
    mov g irq0#l
    irqset
;irq enable
    irqen


;
; main()
;

mainloop:
    mov h hello#h
    mov g hello#l
    call print_string
    jmp mainloop


;
; end main
;
inf:
    jmp inf;
;
; variables
;
offset 0x0e00
hello:
    db "meaningless data...", 10, 0
irqmsg:
    db 10, "irq here", 10, 0
%include print.asm


;
; stack
;

offset 0xd000
stack:

;
; irq function
;
offset 0xf000
irq0:
    ; save cpu state
    pushf
    push a
    push b
    push c
    push d
    push e
    push f
    push g
    push h
    
    mov h irqmsg#h
    mov g irqmsg#l
    call print_string
    
    pop h
    pop g
    pop f
    pop e
    pop d
    pop c
    pop b
    pop a
    popf
    
    irqret






; terminal memory address
offset 0xff10
terminal:


