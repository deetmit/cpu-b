;
; this is a library
;

;
; print_string() 
; regs: H:G  -pointer to string
; uses external labels: terminal
;
; 
; bugs: only G is incremented!

print_string:

loop:
    mov a [H:G]
    mov b 0
    cmpu= 
    jmpif end
    
    mov [terminal] a
    mov b 1
    mov a g
    add
    mov g a
    jmp loop
    
end:    
    ret

