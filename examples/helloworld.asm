;
;init
;

;set stack
    mov h 0xd
    mov g 0x0
    setstack

;
; main
;

    mov g hello#l
    mov h hello#h
    call message



    
    
inf:
    jmp inf;


fun1:
    call print_string
    ret


message:
    
    mov [varG] g
    mov [varH] h

    mov g start_msg#l
    mov h start_msg#h
    call print_string
    
    mov h [varH]
    mov g [varG]
    call print_string
    
    mov g end_msg#l
    mov h end_msg#h
    call print_string
    
    ret

; print_string(H:G)
print_string:

loop:
    mov a [H:G]
    mov b 0
    cmpu= 
    jmpif end
    
    mov [terminal] a
    mov b 1
    mov a g
    add
    mov g a
    jmp loop
    
end:    
    ret





offset 0x100
; string
hello:
db "Hello world!", 10, "welcome Neo on the other side", 10, 0

start_msg:
db "Starting system...", 10, 0

end_msg:
db "Shuting down system...", 10, 0


varG:
    db 0
varH:
    db 0


; terminal memory address
offset 0xff10
terminal:


